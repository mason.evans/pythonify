{
  description = "A flake for pythonification";

  #inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-21.11-darwin";
  #inputs.nixpkgs.follows = "nix/nixpkgs";
  #inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, ... }@inputs:
    let
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);

      getPkgs = system: {
        ppkgs = import nixpkgs {
          inherit system;
        };
      };

      venvDir = "./env";

      runPackages = pypkgs: with pypkgs; [
          python
          venvShellHook
        ];

      devPackages = pypkgs: with pypkgs; [
          #pylint
          flake8
          black
      ];

      # This is to expose the venv in PYTHONPATH so that pylint can see venv packages
      postShellHook = ppkgs: ''
        PYTHONPATH=\$PWD/\${venvDir}/\${ppkgs.python.sitePackages}/:\$PYTHONPATH
        # pip install -r requirements.txt
      '';

      runShell = ppkgs: system: {
        sh = { 
          runShell = ppkgs.mkShell {
            inherit venvDir;
            name = "pythonify-run";
            packages = runPackages ppkgs.python311Packages;
            postShellHook = postShellHook ppkgs.python311Packages;
          };
        };
      };

      developmentShell = ppkgs: system: {
        sh = { 
          developmentShell = ppkgs.mkShell {
            inherit venvDir;
            name = "pythonify-dev";
            packages = (runPackages ppkgs.python311Packages) ++ (devPackages ppkgs.python311Packages);
            postShellHook = postShellHook ppkgs.python311Packages;
          };
        };
      };

    in {
      packages = 
        forAllSystems (system: ((runShell (getPkgs system).ppkgs system).sh) // (developmentShell (getPkgs system).ppkgs system).sh);
        #forAllSystems (system: (developmentShell (getPkgs system).ppkgs system).sh);
    };
}
